/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package basketbase;

import static basketbase.FileEngine.STAT_SEPARATOR;
import java.util.StringTokenizer;

/**
 *
 * @author Frollo
 */
class PlayerStatistics {
    private static final int STAT_FIELDS_N = 11;
    
    private int minutes;
    private Fouls fouls;
    private Shots shots_from_under;
    private Shots shots_from_outside;
    private Shots shots_3;
    private Shots shots_free;
    private Bounce bounces;
    private Balls balls;
    private int as;
    private int pts;
    private int val;
    
    public PlayerStatistics(){
        //Everything starts from 0 (in case we create something new, of course)
        
        this.minutes = 0;
        this.fouls = new Fouls(0 ,0 );
        this.shots_from_under = new Shots(0 , 0);
        this.shots_from_outside = new Shots(0 , 0);
        this.shots_3 = new Shots(0 ,0);
        this.shots_free = new Shots (0 , 0);
        this.bounces = new Bounce (0 ,0);
        this.balls = new Balls (0 ,0);
        this.as = 0;
        this.pts = 0;
        this.val = 0;
    }
    
    public PlayerStatistics(String ps) throws BasketBaseLoadingException{
        //If we are loading, we parse the string and create a new object with the old value
        StringTokenizer st = new StringTokenizer (ps , STAT_SEPARATOR);
        //First thing, we check the string, only a string with a suitable number of fields will be accepted
        if(st.countTokens() != STAT_FIELDS_N){
            BasketBaseLoadingException ld = new BasketBaseLoadingException ("Wrong fields number: " + Integer.toString (st.countTokens()));
            throw ld;
        }else{
            this.minutes = Integer.parseInt(st.nextToken());
            this.fouls = new Fouls (st.nextToken()); //BiAttr objects use their own creation methods
            this.shots_from_under = new Shots (st.nextToken());
            this.shots_from_outside = new Shots (st.nextToken());
            this.shots_3 = new Shots (st.nextToken());
            this.shots_free = new Shots (st.nextToken());
            this.bounces = new Bounce (st.nextToken());
            this.balls = new Balls (st.nextToken());
            this.as = Integer.parseInt(st.nextToken());
            this.pts = Integer.parseInt(st.nextToken());
            this.val = Integer.parseInt(st.nextToken());
        }
    }
    
    @Override
    public String toString(){
        String str = Integer.toString(this.minutes) + STAT_SEPARATOR;
        str += this.fouls.toString() + STAT_SEPARATOR;
        str += this.shots_from_under.toString() + STAT_SEPARATOR;
        str += this.shots_from_outside.toString() + STAT_SEPARATOR;
        str += this.shots_3.toString() + STAT_SEPARATOR;
        str += this.shots_free.toString() + STAT_SEPARATOR;
        str += this.bounces.toString() + STAT_SEPARATOR;
        str += this.balls.toString() + STAT_SEPARATOR;
        str += Integer.toString(this.as) + STAT_SEPARATOR;
        str += Integer.toString(this.pts)+ STAT_SEPARATOR;
        str += Integer.toString(this.val) + STAT_SEPARATOR;
        
        return str;
    }
    
    public void updateStatistics(String str) throws BasketBaseParsingException{
    
    }
}

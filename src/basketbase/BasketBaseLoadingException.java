/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package basketbase;

/**
 *
 * @author Frollo
 */
class BasketBaseLoadingException extends Exception{
    public static final int LOADING_EXCEPTION_PARSE_ERROR = 0;
    
    private String message;
    private int status;
    
    public BasketBaseLoadingException(String string, int s) {
        this.message = string;
        this.status = s; 
    }
    
    public int getStatus(){
        return this.status;
    }
}

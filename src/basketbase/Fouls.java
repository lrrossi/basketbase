/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package basketbase;

/**
 *
 * @author Frollo
 */
class Fouls extends BiAttr{
    public Fouls (int Done, int Suffered){
        super(Done, Suffered);
    }
    
    public Fouls (String parsed){
        super(parsed);
    }
    
    public void setDone (int D){
        this.setA(D);
    }
    
    public void setSuffered(int S){
        this.setB(S);
    }
    
    public void setBoth(int Done, int Suffered){
        this.setA(Done);
        this.setB(Suffered);
    }
    
    public void addDone(int D){
        this.setA (this.A + D);
    }
    
    public void addSuffered (int S){
        this.setB(this.B + S);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package basketbase;

import static basketbase.FileEngine.*;

/**
 *
 * @author Frollo
 */
public class Player {
    private String name; //Player name
    private String surname; //Player surname
    private int number; //Player number
    private PlayerStatistics stats; //A pointer to the statistics
    
    public Player (int n, String surname, String name, Team team){
        this.name = name;
        this.surname = surname;
        this.number = n;
        this.stats = new PlayerStatistics();
        
        team.addPlayer(this);
    }
    
    public String toString(){
    //The strings are formatted this way:
    // Number%Surname%Name%stats
    //To see the stats output go to basketbase.PlayerStatistics
    return(Integer.toString(this.number) + PLAYER_SEPARATOR + this.surname + PLAYER_SEPARATOR + this.name + PLAYER_SEPARATOR + this.stats.toString());
    }
}

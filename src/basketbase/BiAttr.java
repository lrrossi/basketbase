/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package basketbase;

import static basketbase.FileEngine.*;
import java.util.StringTokenizer;

/**
 * A superclass built to manage attributes composed by two integer values
 * @author Frollo
 */
public class BiAttr {
    protected int A;
    protected int B;
    
    
    public BiAttr(int a, int b){
        this.A = a;
        this.B = b;
    }
    
    public BiAttr(String parsed){
        StringTokenizer st = new StringTokenizer (parsed , BIATTR_SEPARATOR);
        int a = Integer.parseInt(st.nextToken());
        int b = Integer.parseInt(st.nextToken());
        this.A = a;
        this.B = b;
    }
    
    public int diff(){
        return this.A - this.B;
    }
    
    public float percentage(){
        float perc = (float) this.A / (float) this.B;
        perc = perc * 100;
        return perc;
    }
    
    public int sum(){
        return A + B;
    }
    
    public void setA(int a){
        this.A = a;
    }
    
    public void setB(int b){
        this.B = b;
    }
    
    
    @Override
    public String toString(){
        //The output is formatted this way: $done!suffered
        return ("$" + Integer.toString(this.A) + BIATTR_SEPARATOR + Integer.toString(this.B));
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package basketbase;

/**
 *
 * @author Frollo
 */
class Shots extends BiAttr{
    
    public Shots (int scored, int tried){
        super(scored, tried);
    }
    
    public Shots (String parsed){
        super(parsed);
    }
    
    public void setScored(int S){
        this.setA(S);
    }
    
    public void setTried(int T){
        this.setB(T);
    }
    
    public void setBoth(int S, int T){
        this.setA(S);
        this.setB(T);
    }
    
    public void addScored(int S){
        this.setA(this.A + S);
        this.setB(this.B + S); //Every scored shot as also been tried
    }
    
    public void addTried(int T){
        this.setB(this.B + T);
    }
}

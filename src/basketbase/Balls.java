/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package basketbase;

/**
 *
 * @author Frollo
 */
class Balls extends BiAttr{
    public Balls (int T, int L){
        super(T, L);
    }
    
    public Balls (String parsed){
        super(parsed);
    }
    
    public void setTaken (int T){
        this.setA(T);
    }
    
    public void setLost(int L){
        this.setB(L);
    }
    
    public void setBoth(int Taken, int Lost){
        this.setA(Taken);
        this.setB(Lost);
    }
    
    public void addTaken(int T){
        this.setA (this.A + T);
    }
    
    public void addLost (int L){
        this.setB(this.B + L);
    }
}
